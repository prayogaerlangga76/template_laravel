<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use Illuminate\Http\Request;
use App\Models\KategoriProduk;
use RealRashid\SweetAlert\Facades\Alert;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::all();
        $kategori = KategoriProduk::all();
        return view('backend.produk.index', [
            'produk' => $produk,
            'title' => 'Kelola Produk',
            'kategori' => $kategori
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.produk.create', [
            'title' => 'Kelola Produk',
            'kategori' => KategoriProduk::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'name' => 'required',
            'produk_kategori_id' => 'required',
            'description' => 'required',
            'harga' => 'required',
            'foto' => 'required|mimes:jpeg,jpg,png',
        ];

        $requestAll = $request->validate($data);

        $produk = new Produk;
        $produk->name = strip_tags($requestAll['name']);
        $produk->produk_kategori_id = $requestAll['produk_kategori_id'];
        $produk->description = strip_tags($requestAll['description']);
        $produk->harga = $requestAll['harga'];
        $$produk->warna_id = mt_rand(1, 4);
        $produk->foto = $requestAll['foto']->getClientOriginalName();
        $requestAll['foto']->move(public_path('file'), $requestAll['foto']->getClientOriginalName());
        $produk->save();

        Alert::success('Berhasil', 'Produk berhasil ditambahkan');

        return redirect('/produk');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
