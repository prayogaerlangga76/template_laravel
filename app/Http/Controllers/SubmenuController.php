<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Submenu;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class SubmenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $submenu = Submenu::paginate(10)->withQueryString();
        return view('backend.submenu.index', [
            'title' => 'Submenu Management',
            'submenu' => $submenu
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menu = Menu::all();
        return view('backend.submenu.create', [
            'menu' => $menu,
            'title' => 'Submenu Management',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAll = $request->validate([
            'title' => 'required',
            'menu_id' => 'required',
            'icon' => 'required',
            'url' => 'required',
            'is_active' => 'required'
        ]);

        $submenu = new Submenu;
        $submenu->title = $requestAll['title'];
        $submenu->menu_id = $requestAll['menu_id'];
        $submenu->icon = $requestAll['icon'];
        $submenu->url = $requestAll['url'];
        $submenu->is_active = $requestAll['is_active'];
        $submenu->save();
        
        Alert::success('Berhasil', 'Submenu baru berhasil ditambahkan');
        return redirect('/submenu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $submenu = Submenu::find($id);
        $menu = Menu::all();
        return view('backend.submenu.edit', [
            'submenu' => $submenu,
            'title' => 'Submenu Management',
            'menu' => $menu
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
        $submenu = Submenu::find($id);
        $requestAll= $request->validate([
            'title' => 'required',
            'menu_id' => 'required',
            'icon' => 'required',
            'url' => 'required',
            'is_active' => 'required'
        ]);

        $submenu->title = $requestAll['title'];
        $submenu->menu_id = $requestAll['menu_id'];
        $submenu->icon = $requestAll['icon'];
        $submenu->url = $requestAll['url'];
        $submenu->is_active = $requestAll['is_active'];
        $submenu->update();

        Alert::success('Berhasil', 'Submenu berhasil diubah');
        return redirect('/submenu');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Submenu::destroy($id);
        Alert::success('Berhasil', 'Submenu berhasil dihapus');
        return redirect('/submenu');
    }
}
