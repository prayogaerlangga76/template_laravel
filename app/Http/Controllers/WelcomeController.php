<?php

namespace App\Http\Controllers;

use App\Models\KategoriProduk;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index()
    {
        $kategori = KategoriProduk::all();
        return view('home.index', [
            'kategori' => $kategori,
        ]);
    }
}
