<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Role::create([
            'name' => 'PJ'
        ]);
        Role::create([
            'name' => 'Root'
        ]);
        Role::create([
            'name' => 'Pembeli'
        ]);
        Role::create([
            'name' => 'Penjual'
        ]);
        Role::create([
            'name' => 'Admin'
        ]);
        
    }
}
