<?php

namespace Database\Seeders;

use App\Models\Bgwarna;
use Illuminate\Database\Seeder;

class WarnaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bgwarna::create([
            'warna' => 'product-item--bg-maya-blue'
        ]);
        Bgwarna::create([
            'warna' => 'product-item--bg-lime-green'
        ]);
        Bgwarna::create([
            'warna' => 'product-item--bg-gold'
        ]);
        Bgwarna::create([
            'warna' => 'product-item--bg-red-orange'
        ]);
    }
}
