<aside class="left-sidebar" data-sidebarbg="skin6">
    @php
        use App\Models\Menu;
        $menu = Menu::all();
    @endphp
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                @foreach ($menu as $m)
                <li class="sidebar-item pt-2">
                        <h6 style="margin-left: 20px; margin-top: 20px;">{{ $m->name }}</h6>
                        @foreach ($m->submenu as $s)
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ $s->url }}">
                                <i class="{{ $s->icon }}" ></i>
                                <span class="hide-menu">{{ $s->title }}</span>
                            </a>
                        @endforeach
                    </li>
                @endforeach

                
            </ul>

        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
