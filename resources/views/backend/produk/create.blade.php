@extends('backend.layouts.main')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-white">
        <div class="row align-items-center">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <form action="/produk" method="post" enctype="multipart/form-data">
                <div class="row">
                @csrf
                <div class="col-md-6 col-lg-6 col-sm-6">
                    <div class="white-box">
                        <div class="d-md-flex mb-3">
                            <h3 class="box-title mb-0">Tambah Produk</h3>
                        </div>
                        <div class="col-lg">

                            <label for="name">Nama Produk</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror"
                                value="{{ old('name') }}" name="name" id="name" autofocus>
                            @error('name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                            <label for="produk_kategori_id" class="mt-4">Kategori Produk</label>
                            <select name="produk_kategori_id" id="produk_kategori_id"
                                class="form-control @error('produk_kategori_id') is-invalid @enderror">
                                <option value="" selected disabled>Pilih Kategori</option>
                                @foreach ($kategori as $k)
                                    @if ($k->id == old('produk_kategori_id'))
                                        <option value="{{ $k->id }}" selected>{{ $k->name }}</option>
                                    @endif
                                    <option value="{{ $k->id }}">{{ $k->name }}</option>
                                @endforeach
                            </select>
                            @error('produk_kategori_id')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                            <label for="description" class="mt-4">Deskripsi Produk</label>
                            <textarea name="description" id="description" cols="30" rows="10"
                                class="form-control @error('description') is-invalid @enderror">{{ old('description') }}</textarea>
                            @error('description')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                            <label for="harga">Harga Produk</label>
                            <input type="number" min="0" class="form-control @error('harga') is-invalid @enderror"
                                name="harga" id="harga" autofocus>
                            @error('harga')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror

                            <button type="submit" class="btn btn-primary mt-3">Tambah</button>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6">
                    <div class="white-box">
                        <div class="d-md-flex mb-3">
                            <h3 class="box-title mb-0"></h3>
                        </div>
                        <div class="col-lg">
                            <div class="custom-file">
                                <label for="foto">Foto Produk</label>
                                <input type="file" class="form-control" id="foto" name="foto" onchange="previewImage()">
                                <img class="img-preview img-fluid mt-2 border rounded">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <!-- ============================================================== -->
        <!-- Recent Comments -->
        <!-- ============================================================== -->

    </div>
@endsection

@push('script')
    <script>
        function previewImage() {
            const image = document.querySelector('#foto');
            const imgPreview = document.querySelector('.img-preview');

            imgPreview.style.display = 'block';

            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function(oFREvent) {
                imgPreview.src = oFREvent.target.result;
            }
        }
    </script>
@endpush