@extends('backend.layouts.main')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-white">
        <div class="row align-items-center">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="white-box">
                    <div class="d-md-flex mb-3">
                        <h3 class="box-title mb-0">Daftar Produk </h3>
                        <div class="col-md-3 col-sm-4 col-xs-6 ms-auto">
                            <a href="/produk/create" class="btn btn-primary shadow-none row border-top">Tambah Produk Baru</a>
                        </div>
                    </div>
                    <div class="row">
                        @forelse ($kategori as $k)
                        <div class="row border-bottom">
                        <h1 class="text-dark mb-0 mb-2 mt-5">{{ Str::upper($k->name) }}</h1>
                    </div>
                        @forelse ($k->produk as $p)
                        <div class="col-lg-4 mt-4">
                            <div class="card border shadow p-3 mb-3 bg-white rounded" style="width: 30rem; max-width: 100%; height: auto;">
                                <img class="card-img-top img-fluid" src="{{ asset('file/' . $p->foto) }}" alt="Card image cap" >
                                <div class="card-body">
                                  <h5 class="card-title">{{ $p->name }}</h5>
                                  <h4 class="card-text box-title">{{ $p->rupiah($p->harga) }}</h4>
                                  <p>{{ Str::words($p->description, 20, '...') }}</p>
                                  <div class="row">
                                    <div class="col-4">
                                    </div>
                                    <div class="col-xs-8">
                                        <a href="#" class="btn btn-info text-white my-2" data-toggle="tooltip" title="Lihat Pro">Lihat Produk</a>
                                        <a href="#" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                                        <a href="#" class="btn btn-danger text-white"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                    
                                    
                                    
                                </div>
                                </div>
                              </div>
                        </div>
                        @empty
                        <h2 class="text-center text-secondary mb-2 mt-4">Belum ada produk</h2>
                        @endforelse
                        
                        
                        @empty
                        <hr>
                            <h2 class="text-center text-secondary">Belum ada produk</h2>
                        @endforelse
                    </div>
                </div>
            </div>
        
        <!-- ============================================================== -->
        <!-- Recent Comments -->
        <!-- ============================================================== -->
        
    </div>
@endsection
 @push('script')
     <script>
        $(function () {
        $('[data-toggle="tooltip"]').tooltip()
        })
     </script>
 @endpush